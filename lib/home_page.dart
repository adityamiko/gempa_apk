import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_application_login/evakuasi.dart';
import 'package:flutter_application_login/informasi.dart';
import 'package:flutter_application_login/laporan.dart';
import 'package:flutter_application_login/maps.dart';
import 'package:google_fonts/google_fonts.dart';

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  void LogOut() async {
    await FirebaseAuth.instance.signOut();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            Stack(
              children: [
                Container(
                    height: 150, width: double.infinity, color: Colors.blue),
                Column(
                  children: [
                    SizedBox(
                      height: 20,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        SizedBox(
                          width: 80,
                        ),
                        Text(
                          "DETEKSI GEMPA DINI",
                          style: GoogleFonts.robotoCondensed(
                            color: Colors.white,
                            fontSize: 23,
                          ),
                        ),
                        SizedBox(
                          width: 30,
                        ),
                        IconButton(
                          iconSize: 30,
                          icon: const Icon(Icons.exit_to_app_rounded),
                          onPressed: LogOut,
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    Container(
                      height: 40,
                      width: 300,
                      decoration: BoxDecoration(
                        color: Color.fromARGB(255, 221, 219, 219),
                        borderRadius: BorderRadius.circular(30),
                      ),
                      child: TextField(
                        cursorHeight: 20,
                        autofocus: false,
                        decoration: InputDecoration(
                            hintText: "Search",
                            prefixIcon: Icon(Icons.search),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30))),
                      ),
                    ),
                    SizedBox(
                      height: 75,
                    ),
                    Container(
                      width: 300,
                      height: 50,
                      child: ElevatedButton.icon(
                        onPressed: () {
                          Navigator.pushReplacement(
                            context,
                            MaterialPageRoute(
                              builder: (context) => Maps(),
                            ),
                          );
                        },
                        icon: Icon(Icons.map_outlined),
                        label: Text("MAPS"),
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Container(
                      width: 300,
                      height: 50,
                      child: ElevatedButton.icon(
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => Evakuasi(),
                            ),
                          );
                        },
                        icon: Icon(Icons.door_back_door_sharp),
                        label: Text("JALUR EVAKUASI"),
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Container(
                      width: 300,
                      height: 50,
                      child: ElevatedButton.icon(
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => Laporan(),
                            ),
                          );
                        },
                        icon: Icon(Icons.note_alt_sharp),
                        label: Text("LAPOR"),
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Container(
                      width: 300,
                      height: 50,
                      child: ElevatedButton.icon(
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => Informasi(),
                            ),
                          );
                        },
                        icon: Icon(Icons.info_outline),
                        label: Text("INFORMASI"),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
