import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_application_login/home_page.dart';
import 'package:google_fonts/google_fonts.dart';

class Evakuasi extends StatelessWidget {
  const Evakuasi({super.key});

  void LogOut() async {
    await FirebaseAuth.instance.signOut();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            Stack(
              children: [
                Container(
                    height: 150, width: double.infinity, color: Colors.blue),
                Column(
                  children: [
                    SizedBox(
                      height: 20,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        IconButton(
                          iconSize: 30,
                          icon: const Icon(Icons.arrow_circle_left_outlined),
                          onPressed: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => HomePage(),
                              ),
                            );
                          },
                        ),
                        SizedBox(
                          width: 30,
                        ),
                        Text(
                          "DETEKSI GEMPA DINI",
                          style: GoogleFonts.robotoCondensed(
                            color: Colors.white,
                            fontSize: 23,
                          ),
                        ),
                        SizedBox(
                          width: 30,
                        ),
                        IconButton(
                          iconSize: 30,
                          icon: const Icon(Icons.exit_to_app_rounded),
                          onPressed: LogOut,
                        ),
                      ],
                    ),
                    Positioned(
                      left: 0,
                      top: 50,
                      child: Align(
                        alignment: Alignment.centerLeft,
                        child: SizedBox(
                          width: 50,
                          height: 19,
                          child: Text(
                            'JALUR EVAKUASI',
                            style: GoogleFonts.roboto(
                              fontSize: 16,
                              fontWeight: FontWeight.w600,
                              height: 1.1725,
                              color: Color(0xff000000),
                            ),
                          ),
                        ),
                      ),
                    ),
                    Positioned(
                      left: 31,
                      top: 114,
                      child: Align(
                        alignment: Alignment.centerLeft,
                        child: SizedBox(
                          width: 251,
                          height: 36,
                          child: Text(
                            'Ikuti petunjuk arah dibawah untuk menuju ke tempat yang aman',
                            style: GoogleFonts.roboto(
                              fontSize: 15,
                              fontWeight: FontWeight.w500,
                              height: 1.1725,
                              color: Color(0xff000000),
                            ),
                          ),
                        ),
                      ),
                    ),
                    Positioned(
                      child: Align(
                        child: SizedBox(
                          width: 20,
                          height: 50,
                          child: Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(40),
                            ),
                          ),
                        ),
                      ),
                    ),
                    Positioned(
                      // more11a54 (212:64)
                      left: 20,
                      top: 19,
                      child: Align(
                        child: SizedBox(
                          width: 500,
                          height: 300,
                          child: Image.network(
                            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSDM4SPPZ1B9PSQceE-bjOvE5Rf2vGw3cHvdw&usqp=CAU',
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
