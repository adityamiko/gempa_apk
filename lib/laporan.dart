import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_application_login/home_page.dart';
import 'package:google_fonts/google_fonts.dart';

class Laporan extends StatelessWidget {
  const Laporan({super.key});

  void LogOut() async {
    await FirebaseAuth.instance.signOut();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            Stack(
              children: [
                Container(
                    height: 150, width: double.infinity, color: Colors.blue),
                Column(
                  children: [
                    SizedBox(
                      height: 20,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        IconButton(
                          iconSize: 30,
                          icon: const Icon(Icons.arrow_circle_left_outlined),
                          onPressed: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => HomePage(),
                              ),
                            );
                          },
                        ),
                        SizedBox(
                          width: 30,
                        ),
                        Text(
                          "DETEKSI GEMPA DINI",
                          style: GoogleFonts.robotoCondensed(
                            color: Colors.white,
                            fontSize: 23,
                          ),
                        ),
                        SizedBox(
                          width: 30,
                        ),
                        IconButton(
                          iconSize: 30,
                          icon: const Icon(Icons.exit_to_app_rounded),
                          onPressed: LogOut,
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Icon(Icons.list_alt_sharp),
                        Text(
                          'LAPOR',
                          style: GoogleFonts.robotoCondensed(
                            fontSize: 23,
                            textStyle: TextStyle(fontWeight: FontWeight.bold),
                          ),
                        ),
                        SizedBox(width: 200),
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      "Hubungi nomor darurat atau tulis laporan!",
                      style: GoogleFonts.robotoCondensed(fontSize: 17),
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Icon(
                          Icons.contact_phone_outlined,
                          size: 50,
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Column(
                          children: [
                            Text(
                              "Daftar Nomor Telepon Darurat",
                              style: GoogleFonts.robotoCondensed(
                                fontSize: 17,
                                textStyle:
                                    TextStyle(fontWeight: FontWeight.bold),
                              ),
                            ),
                            Container(
                              height: 40,
                              width: 220,
                              decoration: BoxDecoration(
                                color: Color.fromARGB(255, 221, 219, 219),
                                borderRadius: BorderRadius.circular(30),
                              ),
                              child: TextField(
                                cursorHeight: 20,
                                autofocus: false,
                                decoration: InputDecoration(
                                    hintText: "Search",
                                    prefixIcon: Icon(Icons.search),
                                    border: OutlineInputBorder(
                                        borderRadius:
                                            BorderRadius.circular(20))),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Container(
                      width: 300,
                      height: 70,
                      decoration: BoxDecoration(
                          color: Colors.lightBlueAccent,
                          borderRadius: BorderRadius.circular(15)),
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          SizedBox(
                            width: 20,
                          ),
                          Icon(
                            Icons.perm_contact_calendar_outlined,
                            size: 50,
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          Column(
                            children: [
                              SizedBox(
                                height: 15,
                              ),
                              Text(
                                "Telpon Darurat Indonesia",
                                style: GoogleFonts.robotoCondensed(
                                  fontSize: 17,
                                ),
                              ),
                              Text(
                                "112",
                                style: GoogleFonts.robotoCondensed(
                                  fontSize: 20,
                                  textStyle:
                                      TextStyle(fontWeight: FontWeight.bold),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Container(
                      width: 300,
                      height: 70,
                      decoration: BoxDecoration(
                          color: Colors.lightBlueAccent,
                          borderRadius: BorderRadius.circular(15)),
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          SizedBox(
                            width: 20,
                          ),
                          Icon(
                            Icons.perm_contact_calendar_outlined,
                            size: 50,
                          ),
                          SizedBox(
                            width: 7,
                          ),
                          Column(
                            children: [
                              SizedBox(
                                height: 15,
                              ),
                              Text(
                                "Damkar",
                                style: GoogleFonts.robotoCondensed(
                                  fontSize: 17,
                                ),
                              ),
                              Text(
                                "113",
                                style: GoogleFonts.robotoCondensed(
                                  fontSize: 20,
                                  textStyle:
                                      TextStyle(fontWeight: FontWeight.bold),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
