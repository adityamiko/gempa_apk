import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class Search extends StatelessWidget {
  const Search({super.key});

  void LogOut() async {
    await FirebaseAuth.instance.signOut();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            Stack(
              children: [
                Container(
                    height: 150, width: double.infinity, color: Colors.blue),
                Column(
                  children: [
                    SizedBox(
                      height: 20,
                    ),
                    Positioned(
                      child: Align(
                        child: (Text(
                          "DETEKSI GEMPA DINI",
                          style: GoogleFonts.robotoCondensed(
                            color: Colors.white,
                            fontSize: 23,
                          ),
                        )),
                      ),
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    Container(
                      height: 40,
                      width: 300,
                      decoration: BoxDecoration(
                        color: Color.fromARGB(255, 221, 219, 219),
                        borderRadius: BorderRadius.circular(30),
                      ),
                      child: Align(
                        alignment: Alignment.centerLeft,
                        child: SizedBox(
                          width: 50,
                          height: 17,
                          child: Text(
                            'Bandung',
                            style: GoogleFonts.robotoCondensed(
                              fontSize: 14,
                              fontWeight: FontWeight.w400,
                              height: 1.1725,
                              color: Color(0xff000000),
                            ),
                          ),
                        ),
                      ),
                    ),
                    Positioned(
                      child: Align(
                        alignment: Alignment.topCenter,
                        child: SizedBox(
                          width: 2000,
                          height: 320,
                          child: Image.network(
                            'https://4.bp.blogspot.com/-ePWdhZ4eYek/T4fcvLomrAI/AAAAAAAAB90/T6dTLUEUqpc/s320/gempa-aceh-2012-ada-gratis-blogspot-com.jpg',
                          ),
                        ),
                      ),
                    ),
                    Text(
                        'Keterangan : Simbol segitiga merah merupakan daerah yang terjadi gempa bumi, Warga dihimbau agar selalu waspada dan apabila terjadi gempa susulan atau terdapat korban segera melaporkan lewat aplikasi ini',
                        style: GoogleFonts.roboto(
                          fontSize: 16,
                          letterSpacing: 1.5,
                          fontWeight: FontWeight.bold,
                        )),
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
