import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_application_login/home_page.dart';
import 'package:google_fonts/google_fonts.dart';

class Maps extends StatelessWidget {
  const Maps({super.key});

  void LogOut() async {
    await FirebaseAuth.instance.signOut();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            Stack(
              children: [
                Container(
                    height: 150, width: double.infinity, color: Colors.blue),
                Column(
                  children: [
                    SizedBox(
                      height: 20,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        IconButton(
                          iconSize: 30,
                          icon: const Icon(Icons.arrow_circle_left_outlined),
                          onPressed: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => HomePage(),
                              ),
                            );
                          },
                        ),
                        SizedBox(
                          width: 30,
                        ),
                        Text(
                          "DETEKSI GEMPA DINI",
                          style: GoogleFonts.robotoCondensed(
                            color: Colors.white,
                            fontSize: 23,
                          ),
                        ),
                        SizedBox(
                          width: 30,
                        ),
                        IconButton(
                          iconSize: 30,
                          icon: const Icon(Icons.exit_to_app_rounded),
                          onPressed: LogOut,
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Icon(Icons.map_sharp),
                        Text(
                          'MAPS',
                          style: GoogleFonts.robotoCondensed(
                            fontSize: 23,
                            textStyle: TextStyle(fontWeight: FontWeight.bold),
                          ),
                        ),
                        SizedBox(width: 200),
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      "Berikut Tampilan titik-titik rawan gempa",
                      style: GoogleFonts.robotoCondensed(fontSize: 17),
                    ),
                    Positioned(
                      child: Align(
                        child: SizedBox(
                          width: 20,
                          height: 50,
                          child: Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(40),
                            ),
                          ),
                        ),
                      ),
                    ),
                    Positioned(
                      // more11a54 (212:64)
                      left: 20,
                      top: 19,
                      child: Align(
                        child: SizedBox(
                          width: 500,
                          height: 200,
                          child: Image.network(
                            'https://asset.kompas.com/crops/TXj0PTCiGdq7NjQ_wTJN49yQvlg=/0x0:1000x667/750x500/data/photo/2018/01/23/27629938015.jpg',
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
