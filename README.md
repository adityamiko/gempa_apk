# GEMPA APK

A Android-basedd project using the Flutter framework for an Earthquake Disaster information system that is connected directly to the API provided by the _Indonesian Meteorology, Climatology and Geophysics Agency_ (**BMKG**).

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://docs.flutter.dev/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://docs.flutter.dev/cookbook)

For help getting started with Flutter development, view the
[online documentation](https://docs.flutter.dev/), which offers tutorials,
samples, guidance on mobile development, and a 
